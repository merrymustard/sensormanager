package com.example.merry.sensormanager;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager manager;
    private Sensor lightSensor;
    private Sensor accSensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensorList = manager.getSensorList(Sensor.TYPE_ALL);
        String features = "";

        for ( Sensor item : sensorList){
            features += item.getName() + "\n";

        }
        Toast.makeText(this, features, Toast.LENGTH_LONG).show();
        Log.d("TAG", features);

        lightSensor = manager.getDefaultSensor(Sensor.TYPE_LIGHT);
        accSensor = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


    }

    @Override
    public void onResume(){
        super.onResume();
        if (lightSensor != null){
            manager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (accSensor != null){
            manager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        if (lightSensor != null){
            manager.unregisterListener(this, lightSensor);
        }
        if (accSensor != null){
            manager.unregisterListener(this, accSensor);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            long eventTime = event.timestamp;
            Toast.makeText(this, String.valueOf(eventTime),
                    Toast.LENGTH_SHORT).show();
        } else if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float[] accSensorValues = event.values;
           // Log.d("TAG", "Acc: " + accSensorValues[0] + "," + accSensorValues[1] + "," + accSensorValues[2]);

            double accMeasure = Math.pow(accSensorValues[0], 2) +
                    Math.pow(accSensorValues[1], 2) +
                    Math.pow(accSensorValues[2], 2);
            Log.d("TAG", "Measures: " + accMeasure);
            int threshold = 5;
            if (accMeasure > threshold){

            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
